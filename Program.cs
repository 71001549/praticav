﻿using System;

namespace praticav
{
    class Program
    {
        static void Main(string[] args)
        {
            double valor = 0;
            double saldo = 0;
            bool showMenu = true;
            while (showMenu)
            {
                Console.WriteLine("================================");
                Console.WriteLine("Selecione uma das opções abaixo:");
                Console.WriteLine("================================");
                Console.WriteLine("1) Criar Conta Corrente");
                Console.WriteLine("2) Depósito");
                Console.WriteLine("3) Saque");
                Console.WriteLine("4) Saldo");
                Console.WriteLine("5) Sair");
                string opcao = Console.ReadLine();
                switch (opcao)
                {
                    case "1":
                        Console.WriteLine("Digite o Nome Completo:");
                        string nome = Console.ReadLine();
                        Console.WriteLine("Digite o CPF:");
                        string cpf = Console.ReadLine();
                        Console.WriteLine("Conta Corrente criada com sucesso! Agencia: 0001 Conta:101010");
                        break;
                    case "2":
                        Console.WriteLine("Digite o valor para depósito:");
                        valor = Convert.ToDouble(Console.ReadLine());
                        if(valor > 0)
                        {
                            saldo = saldo + valor;
                            Console.WriteLine("R$" + valor + " depositado na conta 101010 com sucesso!");
                        }
                        else
                        {
                            Console.WriteLine("Valor inválido! Tente novamente.");    
                        }
                        break;
                    case "3":
                        Console.WriteLine("Digite o valor para saque:");
                        valor = Convert.ToDouble(Console.ReadLine());
                        if(valor > saldo)
                        {   
                            Console.WriteLine("Saldo Insuficiente! Tente novamente.");                            
                        }   
                        else
                        {
                            saldo = saldo-valor;
                            Console.WriteLine("Saque realizado com sucesso! Seu saldo atual é de R$ " + saldo);    
                        }
                        break;
                    case "4":
                        Console.WriteLine("Seu saldo atual é de R$ "+saldo);
                        break;                                                       
                    case "5":
                        showMenu = false;
                        break;
                    default:
                        Console.WriteLine("Escolha uma opção válida");
                        break;
                }
            }
        }
    }
}
